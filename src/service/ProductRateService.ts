import {ProductRate} from '../model/ProductRate';
import ProductDescription from '../model/ProductDescription';
import ProductComponent from '../model/ProductComponent';

export class ProductRateService {

  static rateProduct(product: ProductDescription): ProductRate {
    const compositionsSize = product.composition.length;
    const wholeImpact = product.composition
        .map((product) => ProductRateService.rateComponent(product))
        .reduce((accumulator, currentValue) => accumulator + currentValue)
      / compositionsSize;

    return this.getRateFromImpact(wholeImpact)
  }

  static rateComponent(component: ProductComponent): ProductRate {
    const impact = this.impactOfComponent(component);
    return this.getRateFromImpact(impact)
  }

  static getRateFromImpact(impact: number): ProductRate {
    if (impact >= 0.3) {
      return ProductRate.BAD;
    } else if (impact >= 0.1) {
      return ProductRate.NEUTRAL
    } else {
      return ProductRate.GOOD;
    }
  }

  static impactOfComponent(component: ProductComponent): number {
    return component.distribution * component.impact;
  }
}
