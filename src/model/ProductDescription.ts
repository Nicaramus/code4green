import ProductComponent from "./ProductComponent";

export default interface ProductDescription {
  name: string
  composition: ProductComponent[]
}
