export default interface ProductComponent {
  name: string,
  impact: number,
  distribution: number
}
