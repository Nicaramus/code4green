import React from 'react'
import {View} from "react-native";
import {ProductRate} from "../model/ProductRate";
import AppConstants from "../util/AppConstants.json"

type Props = {
  productRate: ProductRate,
  size: number
}

function getColor(productRate: ProductRate): string {
  switch (productRate) {
    case ProductRate.BAD: return 'red';
    case ProductRate.NEUTRAL: return 'orange';
    case ProductRate.GOOD: return 'green';
  }
}

export default function ImpactMarker(props: Props & {style: any}) {
  const color = getColor(props.productRate);

  const viewStyle = {
    width: props.size,
    height: props.size,
    backgroundColor: color,
    borderWidth: 1,
    borderColor: AppConstants.view.colors.first
  };

  return (
    <View style={{...viewStyle, ...props.style}}/>
  )
}

ImpactMarker.defaultProps = {
  style: {}
};
