import React, {Component} from 'react';
import {
  View,
  StyleSheet,
} from 'react-native';
import {RNCamera as Camera} from 'react-native-camera';
import Barcode from "../model/Barcode";

type Props = {
  onScannedProduct: (barcode: Barcode) => void
}

type State = {
  torchOn: boolean
}

export default class BarcodeScan extends Component<Props, State> {
  state: State = {
    torchOn: false
  };

  onBarCodeRead = (e: any) => {
    this.props.onScannedProduct({value: e.data, type: e.type});
  };

  render() {
    return (
      <View style={styles.container}>
        <Camera
          style={styles.preview}
          onBarCodeRead={(event) => this.onBarCodeRead(event)}
          captureAudio={false}
        >
        </Camera>
      </View>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    width: "100%",
    height: "100%",
    flex: 1,
    flexDirection: 'row',
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  cameraIcon: {
    margin: 5,
    height: 40,
    width: 40
  },
  bottomOverlay: {
    position: "absolute",
    width: "100%",
    flex: 20,
    flexDirection: "row",
    justifyContent: "space-between"
  },
});
