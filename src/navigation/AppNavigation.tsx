import React from "react"
import {createStackNavigator} from "@react-navigation/stack";
import NavigationRoutes from "../navigation/NavigationRoutes"
import MainPage from "../page/MainPage";
import ProductDescriptionPage from "../page/ProductDescriptionPage";
import BarcodeScannerPage from "../page/BarcodeScannerPage";

const Stack = createStackNavigator();

export default function AppNavigation() {
  return (
    <Stack.Navigator initialRouteName={NavigationRoutes.mainPage}>
      <Stack.Screen
        name={NavigationRoutes.mainPage}
        component={MainPage}
        options={{
          headerTitle: "GreenRay"
        }}
      />
      <Stack.Screen
        name={NavigationRoutes.barcodeScannerPage}
        component={BarcodeScannerPage}
        options={{
          headerTitle: "Scan the barcode"
        }}
      />
      <Stack.Screen
        name={NavigationRoutes.productDescriptionPage}
        component={ProductDescriptionPage}
        options={{
          headerTitle: "Product description"
        }}
      />
    </Stack.Navigator>
  )
}
