export default class NavigationRoutes {
  static mainPage = "MAIN_PAGE";
  static barcodeScannerPage = "BARCODE_SCANNER_PAGE";
  static productDescriptionPage = "PRODUCT_DESCRIPTION_PAGE";
}
