import React from "react";
import ProductDescription from "../model/ProductDescription";
import {StyleSheet, Text, View} from "react-native";
import AppConstants from "../util/AppConstants.json";
import ProductComponent from "../model/ProductComponent";
import {ProductRateService} from "../service/ProductRateService";
import ImpactMarker from "../component/ImpactMarker";

type State = {
  productDescription: ProductDescription | null
}

export default class ProductDescriptionPage extends React.Component<any, State> {
  state: State = {
    productDescription: null
  };

  loadProductDescription(): Promise<ProductDescription> {
    return new Promise((resolve) => {
      const prodDescription: ProductDescription = {
        name: "Smartphone",
        composition: [
          {name: "gold", impact: 0.05, distribution: 0.05},
          {name: "glass", impact: 0.1, distribution: 0.20},
          {name: "plastic", impact: 0.05, distribution: 0.6},
          {name: "lit", impact: 0.8, distribution: 0.15},
        ],
      };

      resolve(prodDescription)
    })
  }

  componentDidMount(): void {
    this.loadProductDescription().then(prodDescription => {
      this.setState({productDescription: prodDescription});
    })
  }

  renderImpactValue(comp: ProductComponent): JSX.Element {
    const rateToDisplay = (rate: number) => {
      const rateMultiplied = rate * 100;
      if(rateMultiplied < 1) {
        return (rateMultiplied).toFixed(2)
      } else {
        return Math.round(rateMultiplied)
      }
    };

    const rate = ProductRateService.impactOfComponent(comp);

    return (
      <Text style={styles.componentValue}>{rateToDisplay(rate)}</Text>
    )
  }

  render() {
    const productDescription = this.state.productDescription;
    if (!productDescription) {
      return null
    }

    const productRate = ProductRateService.rateProduct(productDescription);

    const productDescriptionListRender = productDescription.composition.map(comp => {
      const rate = ProductRateService.rateComponent(comp);

      return (
        <View key={comp.name} style={styles.descriptionListRow}>
          {this.renderImpactValue(comp)}
          <ImpactMarker productRate={rate} size={10} style={{marginHorizontal: 10}}/>
          <Text style={styles.descriptionListRowText}>{comp.name}</Text>
        </View>
      )
    });

    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <ImpactMarker productRate={productRate} size={18}/>
          <Text style={styles.headerText}>{productDescription.name}</Text>
        </View>

        <Text style={styles.materialsHeader}>
          Materials (in points of impact):
        </Text>
        {productDescriptionListRender}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
    height: "100%",
    backgroundColor: AppConstants.view.colors.fourth
  },
  header: {
    flexDirection: "row",
    alignItems: 'center',
    margin: 10
  },
  headerText: {
    fontSize: 28,
    fontWeight: "bold",
    marginHorizontal: 10
  },
  descriptionListRow: {
    flexDirection: "row",
    alignItems: 'center',
    marginHorizontal: 10
  },
  componentValue: {
    width: 60,
    textAlign: 'right',
    fontSize: 22
  },
  descriptionListRowText: {
    fontSize: 22
  },
  materialsHeader: {
    fontSize: 18,
    marginHorizontal: 10,
    marginBottom: 10
  }
});
