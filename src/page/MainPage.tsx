import React from "react"
import {Text, TouchableHighlight, View, StyleSheet} from "react-native"
import NavigationRoutes from "../navigation/NavigationRoutes";
import AppConstants from "../util/AppConstants.json"

export default class MainPage extends React.Component<any,any> {
  navigateToBarcodeScanner() {
    this.props.navigation.push(NavigationRoutes.barcodeScannerPage)
  }

  render() {
    return (
      <View style={styles.container}>
        <TouchableHighlight onPress={() => this.navigateToBarcodeScanner()}>
          <Text>Scan</Text>
        </TouchableHighlight>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
    height: "100%",
    backgroundColor: AppConstants.view.colors.fourth
  }
});
