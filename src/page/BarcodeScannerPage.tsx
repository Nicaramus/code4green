import React from "react";
import BarcodeScan from "../component/BarcodeScanner";
import NavigationRoutes from "../navigation/NavigationRoutes";
import Barcode from "../model/Barcode";

export default class BarcodeScannerPage extends React.Component<any, any> {
  onScannedProduct(barcode: Barcode) {
    this.props.navigation.replace(NavigationRoutes.productDescriptionPage)
  }

  render() {
    return (
      <BarcodeScan onScannedProduct={(barcode: Barcode) => this.onScannedProduct(barcode)}/>
    )
  }
}
