import React from 'react';
import {
  SafeAreaView,
} from 'react-native';

import {NavigationContainer} from "@react-navigation/native";
import AppNavigation from "./navigation/AppNavigation";

export default class App extends React.Component {
  render() {
    return (
      <NavigationContainer>
        <SafeAreaView style={{width: "100%", height: "100%"}}>
          <AppNavigation/>
        </SafeAreaView>
      </NavigationContainer>
    )
  }
};
