/**
 * @format
 */

import 'react-native';
import React from 'react';
// Note: test renderer must be required after react-native.
import {ProductRateService} from "../src/service/ProductRateService";
import {ProductRate} from "../src/model/ProductRate";

it('calculates correctly products rates', () => {
    let productRateService = new ProductRateService();

    expect(productRateService.rateProduct({
        name: "Smartphone",
        composition: [
            {name: "gold", impact: 0.05, distribution: 0.05},
            {name: "glass", impact: 0.1, distribution: 0.20},
            {name: "plastic", impact: 0.05, distribution: 0.6},
            {name: "lit", impact: 0.8, distribution: 0.15},
        ],
    })).toBe(ProductRate.NEUTRAL);

    expect(productRateService.rateProduct({
        name: "Bubluzaur",
        composition: [
            {name: "wood", impact: 0.1, distribution: 0.05},
            {name: "glass", impact: 0.2, distribution: 0.20},
            {name: "plastic", impact: 0.1, distribution: 0.5},
            {name: "lit", impact: 0.9, distribution: 0.25},
        ],
    })).toBe(ProductRate.BAD);

    expect(productRateService.rateProduct({
        name: "Doors",
        composition: [
            {name: "wood", impact: 0.01, distribution: 0.9},
            {name: "steel", impact: 0.2, distribution: 0.05},
            {name: "glass", impact: 0.2, distribution: 0.05},
        ],
    })).toBe(ProductRate.GOOD);
});
